#! /bin/bash

#moves ublock extension file into extensions folder
sudo mv uBlock0@raymondhill.net.xpi '/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}' 
echo "Ublock will not work until the computer is restarted"

#adds firefox settings configuration files
sudo mv autoconfig.js /usr/lib/firefox/browser/defaults/preferences
sudo mv mozilla.cfg /usr/lib/firefox

#install chrome
sudo wget -T 10 -nd -nc https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

#adds ublock origin to chrome
sudo mkdir /opt/google/chrome/extensions/
sudo mv cjpalhdlnbpafiamejdnhcphjbkeiagm.json /opt/google/chrome/extensions/

#add chrome shortcut
sudo cp google-chrome.desktop ~/.local/share/applications/google-chrome.desktop

#install gimp
sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp
sudo apt update
sudo apt-get -y install gimp

#install Adobe Reader
wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
sudo apt-get -y install gdebi-core
sudo gdebi --non-interactive Adbe*.deb
sudo apt-get -y install libxml2:i386 libstdc++6:i386 libcanberra-gtk-module libcanberra-gtk3-module
acroread #runs Adobe Reader for first time so that user can accept terms and conditions

#install microsoft fonts
sudo apt install ttf-mscorefonts-installer
sudo fc-cache -f -v
