# Summary
To create a new Ubuntu distribution by producing a combination of custom iso and a preseed configuration file that produces reports of the OS, apps, network and hardware; updates and upgrades the Ubuntu distro and version; implements UEFI booting options; makes accounts and furbish them with goodies; removes extra packages ie. Language packs; adds software packages like chrome, Adobe Reader, vim, gimp, microsoft-fonts; updates apps packages and changes system defaults (theme, icons, desktop background, panels, browser homepage, etc)

# Version
Version Modv3

# Tests
Implement installation of programs with in the ISO image.

# Tools
The software used includes 
squashfs-tools
genisoimage
GIT
Virtualbox
quemu


# System Customizations
Third Account (Public) (Work in Progress)
Kill internal jacks (speakers) (Work in Progress)
Print mfc9970cdwlpr
UEFI booting options


# Added Packages
Google Chrome
Vim
Adobe Acrobat Reader (Work in Progess)
Mozilla Firefox configuration (Work in Progress)
Mozilla Firefox extension Ublock (Work in Progress)
Google Chrome shortcut (Work in progress)
Google Chrome ublock (Work in Progress)
Gimp (Work in Progress)
Microsoft fonts (Work in Progress)

# Removed Packages
firefox-locale-de
firefox-locale-es
firefox-locale-fr
firefox-locale-it
firefox-locale-pt
firefox-locale-ru
firefox-locale-zh-hans
gnome-getting-started-docs-de/disco
gnome-getting-started-docs-es/disco
gnome-getting-started-docs-fr/disco
gnome-getting-started-docs-it/disco
gnome-getting-started-docs-pt/disco
gnome-getting-started-docs-ru/disco
gnome-user-docs-de/disco
gnome-user-docs-es/disco
gnome-user-docs-fr/disco
gnome-user-docs-it/disco
gnome-user-docs-pt/disco
gnome-user-docs-ru/disco
gnome-user-docs-zh-hans/disco
hunspell-de-at-frami/disco
hunspell-de-ch-frami/disco
hunspell-de-de-frami/disco
hunspell-en-au/disco
hunspell-en-gb/disco
hunspell-en-za/disco
hunspell-es/disco
hunspell-fr-classical
hunspell-fr/disco
hunspell-it/disco
hunspell-pt-br/disco
hunspell-pt-pt/disco
hunspell-ru/disco
hyphen-de/disco
hyphen-en-gb/disco
hyphen-es/disco
hyphen-fr/disco
hyphen-it/disco
hyphen-pt-br/disco
hyphen-pt-pt/disco
hyphen-ru/disco
language-pack-de-base/disco
language-pack-de/disco
language-pack-es-base/disco
language-pack-es/disco
language-pack-fr-base/disco
language-pack-fr/disco
language-pack-gnome-de-base/disco
language-pack-gnome-de/disco
language-pack-gnome-es-base/disco
language-pack-gnome-es/disco
language-pack-gnome-fr-base/disco
language-pack-gnome-fr/disco
language-pack-gnome-it-base/disco
language-pack-gnome-it/disco
language-pack-gnome-pt-base/disco
language-pack-gnome-pt/disco
language-pack-gnome-ru-base/disco
language-pack-gnome-ru/disco
language-pack-gnome-zh-hans-base/disco
language-pack-gnome-zh-hans/disco
language-pack-it-base/disco
language-pack-it/disco
language-pack-pt-base/disco
language-pack-pt/disco
language-pack-ru-base/disco
language-pack-ru/disco
language-pack-zh-hans-base/disco
language-pack-zh-hans/disco
libreoffice-help-de/disco
libreoffice-help-en-gb/disco
libreoffice-help-es/disco
libreoffice-help-fr/disco
libreoffice-help-it/disco
libreoffice-help-pt-br/disco
libreoffice-help-pt/disco
libreoffice-help-ru/disco
libreoffice-help-zh-cn/disco
libreoffice-help-zh-tw/disco
libreoffice-l10n-de/disco
libreoffice-l10n-en-gb/disco
libreoffice-l10n-en-za/disco
libreoffice-l10n-es/disco
libreoffice-l10n-fr/disco
libreoffice-l10n-it/disco
libreoffice-l10n-pt-br
libreoffice-l10n-pt/disco
libreoffice-l10n-ru/disco
libreoffice-l10n-zh-cn/disco
libreoffice-l10n-zh-tw/disco
mythes-de-ch/disco
mythes-de/disco
mythes-en-au/disco
mythes-en-us/disco
mythes-es/disco
mythes-fr/disco
mythes-it/disco
mythes-pt-pt/disco
mythes-ru/disco
thunderbird-locale-de/disco
thunderbird-locale-en-gb/disco
thunderbird-locale-es-ar/disco
thunderbird-locale-es-es/disco
thunderbird-locale-es/disco
thunderbird-locale-fr/disco
thunderbird-locale-it/disco
thunderbird-locale-pt-br/disco
thunderbird-locale-pt-pt/disco
thunderbird-locale-pt/disco
thunderbird-locale-ru/disco
thunderbird-locale-zh-cn/disco
thunderbird-locale-zh-hans/disco
thunderbird-locale-zh-hant/disco
thunderbird-locale-zh-tw/disco
wbrazilian/disco
wbritish/disco
wfrench/disco
wportuguese/disco
wspanish/disco
wswiss/disco

