	# Installation related

ls -la /var/log/
sudo cat /var/log/syslog
ls -la /var/log/installer
sudo cat /var/log/installer/casper.log
ls -la /var/log/journal
sudo cat /var/log/dmesg | grep error

ls /var/log/apt
sudo cat /var/log/apt/history.log | grep error
sudo cat /var/log/apt/term.log | grep crash

	#______________________________________________

# List printers
lpinfo 
lpstat -p
lpoptions -l




# Delete a printer
# sudo lpadmin -x HPLaserColor
	#______________________________________________

# update-alternatives
update-alternatives --list
	#______________________________________________

# gsettings
gsettings list-schemas | grep Desktop
gsettings list-recursively | grep Desktop

	## setting org.gnome.desktop.session
gsettings list-recursively | grep org.gnome.desktop.session
	# Desktop Lockdown
gsettings list-recursively | grep org.gnome.desktop.lockdown

	#______________________________________________

	## Desktop Background
#sudo cp fudog.jpeg /usr/share/backgrounds/
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/Cath%C3%A9drale_Marie-Rheine-du-Monde_by_Thierry_Pon.jpg'

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/Definitive_Light_Zen_Orange_by_Pierre_Cante.jpg'

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/Spices_in_Athens_by_Makis_Chourdakis.jpg'

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/Raindrops_On_The_Table_by_Alex_Fazit.jpg'

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/El_Haouaria_by_Nusi_Nusi.jpg'

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/Wall_with_door_on_Gozo_by_Matthias_Niess.jpg'


	#______________________________________________


	# Desktop Lockdown
gsettings list-recursively | grep org.gnome.desktop.lockdown
gsettings set org.gnome.desktop.lockdown disable-lock-screen 'true'
gsettings set org.gnome.desktop.lockdown disable-lock-screen 'false'

	# 5 Minutes
gsettings set org.gnome.desktop.session idle-delay 300

	# 10 Minutes
gsettings set org.gnome.desktop.session idle-delay 600

	# 15 Minutes
gsettings set org.gnome.desktop.session idle-delay 900


