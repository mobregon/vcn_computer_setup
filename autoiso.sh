	##* CUSTOMIZING AND PRESEEDING UBUNTU LIVECDs v2 *##

	#   # Variables ##


originaliso=$"ISOs/ubuntu-18.04.2-desktop-amd64.iso"
originaliso=$"ISOs/ubuntu-19.04-desktop-amd64.iso"

originaliso=$"ISOs/UBUNTU18Modv3.iso"
originaliso=$"ISOs/UBUNTU19Modv3.iso"

mver=$"18Modv7"
mver=$"19v31"

	# ___________________________

	#  1 # First download the ISO you want to start customizing from Ubuntu’s release server.
	
	#  2 # Prerequisite software to be installed.

#sudo apt-get install squashfs-tools genisoimage

	#  3 # Create a fresh folder to begin work. 

#sudo mkdir ~/custom-img

	#  4 # ISOs folder

#sudo mkdir ~/repositories/custom-img/ISOs

	#  5 # Move the base ISO downloaded in the first step to the working directory.

#cp ~/repositories/custom-img/ISOs/$originaliso ~/custom-img


cd ~/custom-img

	#  6 # Extracts the contents of disc image.

sudo mkdir mnt
sudo mkdir extract
sudo mount -o loop $originaliso mnt
sudo rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract

	#  7 # Extracts the filesystem with the following commands:

sudo unsquashfs mnt/casper/filesystem.squashfs
sudo mv squashfs-root edit

	#  8 # Copies resolv.conf from your system into the freshly unpacked fs for network access.

sudo cp /etc/resolv.conf edit/etc/

	#  9 # Mounts a few important working directories.

sudo mount --bind /dev/ edit/dev

	# 10 # CHROOT EDIT HERE (Logical beggining for a second script

sudo chroot edit

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C
dbus-uuidgen > /var/lib/dbus/machine-id
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl

	# 11 # Removes Aps Packages ##

apt-get purge -y mythes-de-ch/disco mythes-de/disco mythes-en-au/disco mythes-en-us/disco mythes-es/disco mythes-fr/disco mythes-it/disco mythes-pt-pt/disco mythes-ru/disco thunderbird-locale-de/disco thunderbird-locale-en-gb/disco thunderbird-locale-es-ar/disco thunderbird-locale-es-es/disco thunderbird-locale-es/disco thunderbird-locale-fr/disco thunderbird-locale-it/disco thunderbird-locale-pt-br/disco thunderbird-locale-pt-pt/disco thunderbird-locale-pt/disco thunderbird-locale-ru/disco thunderbird-locale-zh-cn/disco thunderbird-locale-zh-hans/disco thunderbird-locale-zh-hant/disco thunderbird-locale-zh-tw/disco libreoffice-help-de/disco libreoffice-help-en-gb/disco libreoffice-help-es/disco libreoffice-help-fr/disco libreoffice-help-it/disco libreoffice-help-pt-br/disco libreoffice-help-pt/disco libreoffice-help-ru/disco libreoffice-help-zh-cn/disco libreoffice-help-zh-tw/disco libreoffice-l10n-de/disco libreoffice-l10n-en-gb/disco libreoffice-l10n-en-za/disco libreoffice-l10n-es/disco libreoffice-l10n-fr/disco libreoffice-l10n-it/disco libreoffice-l10n-pt-br libreoffice-l10n-pt/disco libreoffice-l10n-ru/disco libreoffice-l10n-zh-cn/disco libreoffice-l10n-zh-tw/disco language-pack-de-base/disco language-pack-de/disco language-pack-es-base/disco language-pack-es/disco language-pack-fr-base/disco language-pack-fr/disco language-pack-gnome-de-base/disco language-pack-gnome-de/disco language-pack-gnome-es-base/disco language-pack-gnome-es/disco language-pack-gnome-fr-base/disco language-pack-gnome-fr/disco language-pack-gnome-it-base/disco language-pack-gnome-it/disco language-pack-gnome-pt-base/disco language-pack-gnome-pt/disco language-pack-gnome-ru-base/disco language-pack-gnome-ru/disco language-pack-gnome-zh-hans-base/disco language-pack-gnome-zh-hans/disco language-pack-it-base/disco language-pack-it/disco language-pack-pt-base/disco language-pack-pt/disco language-pack-ru-base/disco language-pack-ru/disco language-pack-zh-hans-base/disco language-pack-zh-hans/disco wbrazilian/disco wbritish/disco wfrench/disco wportuguese/disco wspanish/disco wswiss/disco hunspell-de-at-frami/disco hunspell-de-ch-frami/disco hunspell-de-de-frami/disco hunspell-en-au/disco hunspell-en-gb/disco hunspell-en-za/disco hunspell-es/disco hunspell-fr-classical hunspell-fr/disco hunspell-it/disco hunspell-pt-br/disco hunspell-pt-pt/disco hunspell-ru/disco hyphen-de/disco hyphen-en-gb/disco hyphen-es/disco hyphen-fr/disco hyphen-it/disco hyphen-pt-br/disco hyphen-pt-pt/disco hyphen-ru/disco gnome-user-docs-de/disco gnome-user-docs-es/disco gnome-user-docs-fr/disco gnome-user-docs-it/disco gnome-user-docs-pt/disco gnome-user-docs-ru/disco gnome-user-docs-zh-hans/disco gnome-getting-started-docs-de/disco gnome-getting-started-docs-es/disco gnome-getting-started-docs-fr/disco gnome-getting-started-docs-it/disco gnome-getting-started-docs-pt/disco gnome-getting-started-docs-ru/disco

dpkg -P firefox-locale-de firefox-locale-es firefox-locale-fr firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans

dpkg -P mythes-de
dpkg -P mythes-de mythes-de-ch mythes-en-au mythes-fr mythes-it mythes-pt-pt mythes-ru

	# 12 # Adds Multi Arch Support ## (testing)

dpkg --add-architecture i386
dpkg --add-architecture x86
dpkg --add-architecture x64
dpkg --add-architecture x86-64


	# 13 # Updates the software repositories and upgrades the remaining packages on the system.

apt-get update -y && apt-get upgrade -y

	# 14 # Add packages ##

		#  .1 # Downloads and installs Vim

apt-get install -y vim

		#  .2 # Downloads and installs Google Chrome browser

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y ./google-chrome*.deb

		#  .3 # Downloads Adobe Reader

wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb

		#  .4 # Downloads Team Viewer

wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb

		#  .5 # Downloads Printers drivers

wget http://www.brother.com/pub/bsc/linux/packages/mfc9970cdwlpr-1.1.1-5.i386.deb

apt-get autoremove && apt-get autoclean

	# 15 # CHROOT CLEAN UP ##

rm -rf /tmp/* ~/.bash_history
rm /var/lib/dbus/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl

	# 16 # Unmount the directories from the beginning of this guide:

umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
exit
sudo umount edit/dev

	# 17 # Generate a new file manifest:

sudo chmod +w extract/casper/filesystem.manifest

sudo chroot edit dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee extract/casper/filesystem.manifest

sudo cp extract/casper/filesystem.manifest extract/casper/filesystem.manifest-desktop

sudo sed -i '/ubiquity/d' extract/casper/filesystem.manifest-desktop

sudo sed -i '/casper/d' extract/casper/filesystem.manifest-desktop

	# 18 # Compress the filesystem:

sudo mksquashfs edit extract/casper/filesystem.squashfs -b 1048576

	# 19 # Update filesystem size (needed by the installer):

printf $(sudo du -sx --block-size=1 edit | cut -f1) | sudo tee extract/casper/filesystem.size

	# 20 # Delete the old md5sum:

cd extract
sudo rm md5sum.txt

	# 21 # Generate a fresh md5sum: (single command, copy and paste in one piece)

find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt


	## PRESEED FILE ___________________

	# 22 # Preseed file opens with nano.

sudo nano ~/repositories/custom-img/extract/isolinux/isolinux.cfg

	# 23 # manually pastes the following snippet of code into nano
default live-install
label live-install
  menu label ^Install Ubuntu
  kernel /casper/vmlinuz
  append  file=/cdrom/ks.preseed auto=true priority=critical debian-installer/locale=en_US keyboard-configuration/layoutcode=us ubiquity/reboot=true languagechooser/language-name=English countrychooser/shortlist=US localechooser/supported-locales=en_US.UTF-8 boot=casper automatic-ubiquity initrd=/casper/initrd quiet splash noprompt noshell ---

	# 24 # Copied custom pressed file from the ~/repositories/vcn_computer_setup/ks.preseed to the root of the extract/

sudo cp ~/repositories/vcn_computer_setup/ks.preseed ~/repositories/custom-img/extract/

	# 25 # Creates the ISO with genisoimage.

sudo genisoimage -D -r -V "UBUNTU$mver" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ~/repositories/custom-img/ISOs/UBUNTU$mver.iso .

	# 26 # Iso Hybrid command (to boot)
sudo isohybrid ~/repositories/custom-img/ISOs/UBUNTU$mver.iso

	# 27 # Single boot writing ISO image to target USB disk (will destroy data on USB disk):

sudo multibootusb -c -r -i ~/repositories/custom-img/ISOs/UBUNTU$mver.iso -t /dev/sdb


	# CLEAN UP ___________

	#   # umount /dev/loop** and Removing Files
sudo rm -rf ~/repositories/custom-img/extract/ks.preseed
sudo umount /dev/loop18
sudo rm -rf ~/repositories/custom-img/mnt
sudo rm -rf ~/repositories/custom-img/extract
sudo rm -rf ~/repositories/custom-img/edit

